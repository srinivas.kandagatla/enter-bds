import keyboard
import serial

def get_bds_menu(ser):
    print("hit Home key to enter into BDS(Boot Device Selection) Menu or hit End to terminate this program\n")
    while True:
        if keyboard.read_key() == "home":
            print("\n")
            ser.write(b'\033\133\110')     # write a magic string for entering bds
        if keyboard.read_key() == "end":
            break

def main():
    serialport = input("Enter Serial Port device ex: /dev/ttyUSB0: ")
    print("Using " + serialport + " as BDS serial")
    ser = serial.Serial(serialport, 115200, timeout=0, parity=serial.PARITY_NONE, rtscts=0)
    get_bds_menu(ser)
    ser.close()

main()
